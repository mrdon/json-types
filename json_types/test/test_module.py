import json
from enum import Enum
from typing import Set, List, Optional, Union
from unittest import TestCase

import json_types


class Bar:
    def __init__(self, id: int, notype = None):
        self.notype = notype
        self.id = id


class Foo:
    def __init__(self, bar: Bar, name: str):
        self.bar = bar
        self.name = name


class SetFoo:
    def __init__(self, bar: Set[str]):
        self.bar = bar


class ListFoo:
    def __init__(self, bar: List[str]):
        self.bar = bar


class ListWithSetFoo:
    def __init__(self, bar: List[SetFoo]):
        self.bar = bar


class TestModule(TestCase):

    def test_encode(self):
        self.assertEquals({"name": "baz", "bar": {"id": 5, "notype": None}},
                          json.loads(json_types.encodes(Foo(Bar(5), "baz"))))

    def test_encode_not_none(self):
        self.assertEquals({"name": "baz", "bar": {"id": 5}},
                          json.loads(json_types.encodes(Foo(Bar(5), "baz"), exclude_none=True)))

    def test_decode(self):
        foo = json_types.decodes(json.dumps({"name": "baz", "bar": {"id": 5}}), Foo)
        self.assertEquals("baz", foo.name)
        self.assertEquals(5, foo.bar.id)
        self.assertIsNone(foo.bar.notype)

    def test_encode_set(self):
        self.assertEquals({"bar": ["baz"]}, json.loads(json_types.encodes(SetFoo({"baz"}), set_as_list=True)))

    def test_decode_set(self):
        foo = json_types.decodes(json.dumps({"bar": ["baz"]}), SetFoo)
        self.assertEquals({"baz"}, foo.bar)

    def test_encode_list(self):
        self.assertEquals({"bar": ["baz"]}, json.loads(json_types.encodes(ListFoo(["baz"]))))

    def test_decode_list(self):
        foo = json_types.decodes(json.dumps({"bar": ["baz"]}), ListFoo)
        self.assertEquals(["baz"], foo.bar)

    def test_decode_list_with_type(self):
        foo = json_types.decodes(json.dumps({"bar": [{"bar": ["baz"]}]}), ListWithSetFoo)
        self.assertEquals({"baz"}, foo.bar[0].bar)
        self.assertTrue(isinstance(foo.bar[0].bar, set))

    def test_decode_function(self):
        def foo(bar: str):
            return bar
        kwargs = json_types.decodes(json.dumps({"bar": "baz"}), foo)
        result = foo(**kwargs)
        self.assertEquals("baz", result)

    def test_decode_optional(self):
        def foo(name: str, bar: Optional[str]):
            return "{}:{}".format(name, bar)
        kwargs = json_types.decodes(json.dumps({"name": "foo"}), foo)
        result = foo(**kwargs)
        self.assertEquals("foo:None", result)

        kwargs = json_types.decodes(json.dumps({"name": "foo", "bar": "baz"}), foo)
        result = foo(**kwargs)
        self.assertEquals("foo:baz", result)

    def test_decode_union(self):
        def foo(bar: Union[Bar, bool]):
            return bar
        kwargs = json_types.decodes(json.dumps({"bar": True}), foo)
        result = foo(**kwargs)
        self.assertTrue(result is True)

        kwargs = json_types.decodes(json.dumps({"bar": {"id": 4}}), foo)
        result = foo(**kwargs)
        self.assertEquals(4, result.id)


class MyEnum(Enum):
    foo = 1,
    bar = 2


class EnumObj:
    def __init__(self, e: MyEnum):
        self.e = e


class EnumTest(TestCase):

    def test_encode(self):
        self.assertEquals({"e": "foo"}, json.loads(json_types.encodes(EnumObj(MyEnum.foo))))

    def test_decode(self):
        foo = json_types.decodes(json.dumps({"e": "foo"}), EnumObj)
        self.assertEquals(MyEnum.foo, foo.e)
