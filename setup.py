"""
json-types
-------------
"""
from setuptools import setup

setup(
    name='json-types',
    version='0.1.dev0',
    url='https://bitbucket.org/mrdon/json-types',
    license='APLv2',
    author='Don Brown',
    author_email='mrdon@twdata.org',
    description='Encode and decode JSON into and from objects with 3.5 type hinting',
    long_description=__doc__,
    python_requires=">3.7",
    packages=['json_types'],
    include_package_data=True,
    platforms='any',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
